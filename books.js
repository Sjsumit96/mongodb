const { find } = require('lodash');
const client = require('./db')

const db = client.db('itcdb');
const collection = db.collection('books');

const add = async (book) => {
    console.log(book)
    await collection.insertOne(book)
    client.close()
}

const remove = async (bid) => {
    await collection.deleteOne({ "id": bid })
    client.close()
}


const list = async () => {
    const full=await collection.find({}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
    client.close()
})}

module.exports = { add, remove, list }